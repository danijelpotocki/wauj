-- ROLE seed
	INSERT INTO role (title) VALUES ('ROLE_ADMIN'), ('ROLE_USER');
	
-- USER seed - password admin - belongs to role ROLE_ADMIN
	INSERT INTO user (email, name, surname, oib, address, password, unique_code, confirmation)
	VALUES ('admin@wauj.com', 'Admin', 'Prvi', '012345', 'Adresa', '$2a$11$G68QbGjrBO3yWA/ddJmmD.yyTyufra/mRrSkFIApwyohbgs30UKsu', '1', '1');
	
-- USER seed - password user - belongs to role ROLE_USER
	INSERT INTO user (email, name, surname, oib, address, password, unique_code, confirmation)
	VALUES ('user+1@wauj.com', 'User', '1', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '1', '1'),
		('user+2@wauj.com', 'User', '2', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '2', '1'),
		('user+3@wauj.com', 'User', '3', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '3', '1'),
		('user+4@wauj.com', 'User', '4', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '4', '1'),
		('user+5@wauj.com', 'User', '4', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '5', '1'),
		('user+6@wauj.com', 'User', '5', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '6', '1'),
		('user+7@wauj.com', 'User', '6', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '7', '1'),
		('user+8@wauj.com', 'User', '7', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '8', '1'),
		('user+9@wauj.com', 'User', '8', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '9', '1'),
		('user+10@wauj.com', 'User', '9', '012345', 'Adresa', '$2a$11$aYAoZVzxXVuEaIeQbCsGkeCIRn/LDNWOT7CCQfwIbv3AbZnjjCDRa', '10', '1');
		
-- USER_ROLE seed for role ROLE_ADMIN
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'admin@wauj.com' AND title = 'ROLE_ADMIN';
	
-- USER_ROLE seed for role ROLE_USER
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+1@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+2@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+3@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+4@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+5@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+6@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+7@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+8@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+9@wauj.com' AND title = 'ROLE_USER';
	INSERT INTO user_role(user_id, role_id)
		SELECT user.user_id, role.role_id FROM user, role WHERE email = 'user+10@wauj.com' AND title = 'ROLE_USER';
		
INSERT INTO election(election_id, title, description, start_date, end_date)	
VALUES(10000, 'Izbor 1', 'Opis 1', '2015-05-03 00:00:00', '2015-05-08 00:00:00');

INSERT INTO election(election_id, title, description, start_date, end_date)
VALUES(10001, 'Izbor 2', 'Opis 2', '2015-04-13 00:00:00', '2015-04-25 00:00:00');

INSERT INTO election(election_id, title, description, start_date, end_date)
VALUES(10002, 'Izbor 3', 'Opis 3', '2015-04-25 00:00:00', '2015-05-01 00:00:00');

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10000, 'Opcija 1/1', 2, 10000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10001, 'Opcija 1/2', 1, 10000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10002, 'Opcija 1/3', 2, 10000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10003, 'Opcija 1/4', 1, 10000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10005, 'Opcija 2/1', 1, 10001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10006, 'Opcija 2/2', 4, 10001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10007, 'Opcija 2/3', 1, 10001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10008, 'Opcija 3/1', 3, 10002);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(10009, 'Opcija 3/2', 2, 10002);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10000, 1, 10000, '2015-05-04 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10001, 2, 10000, '2015-05-05 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10002, 3, 10000, '2015-05-06 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10003, 4, 10000, '2015-05-07 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10004, 5, 10000, '2015-05-05 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10005, 6, 10000, '2015-05-06 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10006, 1, 10001, '2015-04-14 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10007, 2, 10001, '2015-04-15 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10008, 3, 10001, '2015-04-16 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10009, 4, 10001, '2015-04-17 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10010, 5, 10001, '2015-04-18 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10011, 6, 10001, '2015-04-19 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10012, 1, 10002, '2015-04-26 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10013, 2, 10002, '2015-04-27 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10014, 3, 10002, '2015-04-28 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10015, 4, 10002, '2015-04-29 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(10016, 5, 10002, '2015-04-30 00:00:00');
		
INSERT INTO election(election_id, title, description, start_date, end_date)	
VALUES(20000, 'Izbor 4', 'Opis 4', '2015-05-03 00:00:00', '2015-06-25 00:00:00');

INSERT INTO election(election_id, title, description, start_date, end_date)
VALUES(20001, 'Izbor 5', 'Opis 5', '2015-05-13 00:00:00', '2015-07-01 00:00:00');

INSERT INTO election(election_id, title, description, start_date, end_date)
VALUES(20002, 'Izbor 6', 'Opis 6', '2015-04-20 00:00:00', '2015-06-22 00:00:00');

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20000, 'Opcija 4/1', 1, 20000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20001, 'Opcija 4/2', 0, 20000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20002, 'Opcija 4/3', 1, 20000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20003, 'Opcija 4/4', 0, 20000);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20004, 'Opcija 5/1', 0, 20001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20005, 'Opcija 5/2', 3, 20001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20006, 'Opcija 5/3', 0, 20001);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20007, 'Opcija 6/1', 2, 20002);

INSERT INTO election_option(election_option_id, title, votes, election_id)
VALUES(20008, 'Opcija 6/2', 1, 20002);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20000, 1, 20000, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20001, 2, 20000, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20002, 3, 20000, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20003, 4, 20000, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20004, 5, 20000, '2015-05-05 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20005, 6, 20000, '2015-05-06 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20006, 1, 20001, '2015-05-14 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20007, 2, 20001, '2015-05-15 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20008, 3, 20001, '2015-05-16 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20009, 4, 20001, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20010, 5, 20001, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20011, 6, 20001, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20012, 1, 20002, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20013, 2, 20002, null);

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20014, 3, 20002, '2015-04-28 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20015, 4, 20002, '2015-04-29 00:00:00');

INSERT INTO election_user(election_user_id, user_id, election_id, voted)
VALUES(20016, 5, 20002, '2015-04-30 00:00:00');
		