package wauj.web.dao;

import java.util.List;

import wauj.web.security.Election;
import wauj.web.security.FinishedElection;

public interface IElectionDao {
	
	public void save(Election election);
	public void update(Election election);
	public void delete(int electionId);
	public Election get(int electionId);
	public List<Election> list();
	public List<Election> listActive();
	public List<FinishedElection> listFinished();
}