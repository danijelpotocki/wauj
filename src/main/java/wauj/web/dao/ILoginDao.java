package wauj.web.dao;

public interface ILoginDao {

	public boolean isPasswordCorrect(String email, String password);
}