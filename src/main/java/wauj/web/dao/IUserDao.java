package wauj.web.dao;

import java.util.List;

import wauj.web.security.User;

public interface IUserDao {
	public User get(String email);
	public void addUser(User user);
	void updateUser(User user);
	User getUserById(int id);
	void deleteUser(int id);
	List<User> listUser();
	User getUserByCode(String code);
}