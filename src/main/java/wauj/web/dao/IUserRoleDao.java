package wauj.web.dao;

import wauj.web.model.UserRole;

public interface IUserRoleDao {

	void addUserRole(UserRole userRole);

}
