package wauj.web.dao;

import java.util.List;

import wauj.web.security.ElectionOption;

public interface IElectionOptionDao {
	public List<ElectionOption> add(ElectionOption option);
	public List<ElectionOption> delete(int electionOptionId);
	public List<ElectionOption> list(int electionId);
	public List<ElectionOption> listOptionsFor(int electionId); 
	public void addVote(int electionOptionId);; 
	public ElectionOption get(int electionOptionId);
}
