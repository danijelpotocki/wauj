package wauj.web.dao;

import wauj.web.security.Role;
import wauj.web.security.User;

public interface IRoleDao {
	public Role getRoleForUser(String email);

	void setRoleForUser(User user, String roleTitle);

	Role getRoleByTitle(String roleTitle);
}