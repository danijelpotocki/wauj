package wauj.web.dao.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionOptionDao;
import wauj.web.model.ElectionUser;
import wauj.web.security.Election;
import wauj.web.security.ElectionOption;

public class ElectionOptionDaoImpl implements IElectionOptionDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	private static String getElectionOptionList = "SELECT election_option_id AS electionOptionId, title, votes, election_id as electionId FROM election_option";
	private static String getElectionOptionById = "SELECT election_option_id AS electionOptionId, title, votes, election_id as electionId FROM election_option WHERE election_option_id = ?";
	
	@Transactional(readOnly = true)
	@Override
	@SuppressWarnings("unchecked")
	public List<ElectionOption> list(int electionId) {
		List<ElectionOption> electionOptionsList = sessionFactory.getCurrentSession()
				.createSQLQuery(getElectionOptionList + " WHERE election_id = " + electionId).list();

		return electionOptionsList;
	}
	
	@Override
	public List<ElectionOption> add(ElectionOption option) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(option);
		return list(option.getElectionId());
	}

	@Override
	public List<ElectionOption> delete(int electionOptionId) {
		// TODO Auto-generated method stub
		ElectionOption option = (ElectionOption) sessionFactory.getCurrentSession()
				.load(ElectionOption.class, new Integer(electionOptionId));
		if (option != null) {
			this.sessionFactory.getCurrentSession().delete(option);
		}
		
		this.sessionFactory.getCurrentSession().flush();
		return list(option.getElectionId());
	}

	@Transactional(readOnly = true)
	@Override
	@SuppressWarnings("unchecked")
	public List<ElectionOption> listOptionsFor(int electionId) {
				
		List<ElectionOption> electionOptionsList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getElectionOptionList + " WHERE election_id = " + electionId)
				.setResultTransformer(Transformers.aliasToBean(ElectionOption.class))
				.list();
				
		return electionOptionsList;
	}

	@Override
	public void addVote(int electionOptionId)
	{
		Session session = this.sessionFactory.getCurrentSession();      
		ElectionOption electionOption = (ElectionOption) session.get(ElectionOption.class, new Integer(electionOptionId));
		
        electionOption.setVotes(electionOption.getVotes() + 1);

        session.update(electionOption);
	}

	@Override
	public ElectionOption get(int electionOptionId) {
		Object electionOption = sessionFactory.getCurrentSession()
				.createSQLQuery(getElectionOptionById)
				.setResultTransformer(Transformers.aliasToBean(ElectionOption.class))
				.setParameter(0, electionOptionId)
				.uniqueResult();
		
		return electionOption != null ? (ElectionOption)electionOption : null;
	}
}
