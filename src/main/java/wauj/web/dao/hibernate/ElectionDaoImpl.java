package wauj.web.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionDao;
import wauj.web.security.Election;
import wauj.web.security.FinishedElection;

public class ElectionDaoImpl implements IElectionDao {

	private static final Logger _log = LoggerFactory.getLogger(ElectionDaoImpl.class);

	
	@Autowired
	private SessionFactory sessionFactory;

	private static String getElectionList = "SELECT election_id AS electionId, title, description, start_date as startDate, end_date as endDate FROM election";
	private static String getFinishedElectionList1 = "SELECT election.election_id AS electionId, election.title AS title, election.description AS description, election.start_date as startDate, election.end_date as endDate, election_option.election_option_id AS electionOptionId, election_option.title AS electionOptionTitle, MAX(election_option.votes) AS votes FROM election JOIN election_option ON election.election_id = election_option.election_id WHERE end_date < ";
	private static String getFinishedElectionList2 = " GROUP BY election.election_id";
	
	@Transactional(readOnly = true)
	@Override
	@SuppressWarnings("unchecked")
	public List<Election> list() {
		List<Election> electionsList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getElectionList)
				.setResultTransformer(Transformers.aliasToBean(Election.class))
				.list();
				
		return electionsList;
	}
	
	@Transactional(readOnly = true)
	@Override
	@SuppressWarnings("unchecked")
	public List<Election> listActive() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		
		List<Election> electionsList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getElectionList + " WHERE end_date > " + strDate + " AND start_date < " + strDate)
				.setResultTransformer(Transformers.aliasToBean(Election.class))
				.list();
				
		return electionsList;
	}
	
	@Transactional(readOnly = true)
	@Override
	@SuppressWarnings("unchecked")
	public List<FinishedElection> listFinished() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		
		List<FinishedElection> electionsList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getFinishedElectionList1 + strDate + getFinishedElectionList2)
				.setResultTransformer(Transformers.aliasToBean(FinishedElection.class))
				.list();
				
		return electionsList;
	}
	
	@Override
	public void save(Election election) {
		this.sessionFactory.getCurrentSession().save(election);
	}
	
	@Override
	public void update(Election election) {
		this.sessionFactory.getCurrentSession().update(election);
	}

	@Override
	public void delete(int electionId) {
		Election election = (Election) sessionFactory.getCurrentSession()
	    		.load(Election.class, new Integer(electionId));
	    if (null != election) {
	    	this.sessionFactory.getCurrentSession().delete(election);
	    }
	}

	@Transactional(readOnly = true)
	@Override
	public Election get(int electionId) {   
        
		Session session = this.sessionFactory.getCurrentSession();      
		Election election = (Election) session.get(Election.class, new Integer(electionId));
		
        return election;
	}

}
