package wauj.web.dao.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.controller.HomeController;
import wauj.web.dao.IUserDao;
import wauj.web.security.User;

public class UserDaoImpl implements IUserDao {

	private static final Logger _log = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	private static String getUserByEmail = "SELECT user_id AS userId, email, name, surname, oib, address, password, unique_code AS uniqueCode, confirmation, is_confirmed AS isConfirmed FROM user WHERE email = ?";
	private static String getUserByCode = "SELECT user_id AS userId, email, name, surname, oib, address, password, unique_code AS uniqueCode, confirmation, is_confirmed AS isConfirmed FROM user WHERE confirmation = ?";
	
	private static String getUserList = "SELECT user_id AS userId, email, name, surname, oib, address, password, unique_code AS uniqueCode, confirmation, is_confirmed AS isConfirmed FROM user";

	@Transactional(readOnly = true)
	@Override
	public User get(String email) {
		Object user = sessionFactory.getCurrentSession()
				.createSQLQuery(getUserByEmail)
				.setResultTransformer(Transformers.aliasToBean(User.class))
				.setParameter(0, email)
				.uniqueResult();

		return user != null ? (User) user : null;
	}
	
    @Override
    public User getUserById(int id) {
        Session session = this.sessionFactory.getCurrentSession();      
        User user = (User) session.get(User.class, new Integer(id));
        _log.debug("User loaded successfully, User details="+ user);
        return user;
    }
    
    @Override
    public void addUser(User user) {
        this.sessionFactory.getCurrentSession().save(user);
        _log.debug("User saved successfully, User Details=" + user);
    }
    
    @Override
    public void updateUser(User user) {
    	this.sessionFactory.getCurrentSession().update(user);
        _log.debug("User updated successfully, User Details="+ user);
    }
    
    @Override
    public void deleteUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, new Integer(id));
        if(null != user){
            session.delete(user);
        }
        _log.debug("User deleted successfully, User details=" + user);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<User> listUser() {
		List<User> usersList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getUserList)
				.setResultTransformer(Transformers.aliasToBean(User.class))
				.list();
						
        for(User user : usersList){
        	 _log.debug("Users List::" + user);
        }
        return usersList;
    }
    
    @Override
    public User getUserByCode(String code) {
		Object user = sessionFactory.getCurrentSession()
				.createSQLQuery(getUserByCode)
				.setResultTransformer(Transformers.aliasToBean(User.class))
				.setParameter(0, code)
				.uniqueResult();

		return user != null ? (User) user : null;
    }
}