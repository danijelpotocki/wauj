package wauj.web.dao.hibernate;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import wauj.web.controller.HomeController;
import wauj.web.dao.IUserRoleDao;
import wauj.web.model.UserRole;

public class UserRoleDaoImpl implements IUserRoleDao {

	private static final Logger _log = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
    @Override
    public void addUserRole(UserRole userRole) {
        this.sessionFactory.getCurrentSession().save(userRole);
        _log.debug("UserRole saved successfully, UserRole Details=" + userRole);
    }
}
