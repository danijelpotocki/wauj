package wauj.web.dao.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IRoleDao;
import wauj.web.model.UserRole;
import wauj.web.security.Role;
import wauj.web.security.User;

public class RoleDaoImpl implements IRoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static String getUserRolesUsingEmail = "SELECT r.role_id AS roleId, r.title AS title FROM user u INNER JOIN user_role ur ON ur.user_id = u.user_id INNER JOIN role r ON r.role_id = ur.role_id WHERE u.email = ?";
	private static String getRoleUsingTitle = "SELECT r.role_id AS roleId, r.title AS title FROM role r WHERE r.title = ?";


	@Transactional(readOnly = true)
	@Override
	public Role getRoleForUser(String email) {
		Object role = sessionFactory.getCurrentSession()
				.createSQLQuery(getUserRolesUsingEmail)
				.setResultTransformer(Transformers.aliasToBean(Role.class))
				.setParameter(0, email)
				.uniqueResult();

		return role != null ? (Role) role : null;
	}
	
	@Override
	public void setRoleForUser(User user, String roleTitle) {
		Object roleObject = sessionFactory.getCurrentSession()
				.createSQLQuery(getRoleUsingTitle)
				.setResultTransformer(Transformers.aliasToBean(Role.class))
				.setParameter(0, roleTitle)
				.uniqueResult();
		
		Role role = (Role) roleObject;
		
		Integer roleId = role.getRoleId();
		Integer userId = user.getUserId();
		
		UserRole userRole = new UserRole(roleId, userId);

	}
	
	@Transactional(readOnly = true)
	@Override
	public Role getRoleByTitle(String roleTitle){
		Object role = sessionFactory.getCurrentSession()
				.createSQLQuery(getRoleUsingTitle)
				.setResultTransformer(Transformers.aliasToBean(Role.class))
				.setParameter(0, roleTitle)
				.uniqueResult();

		return role != null ? (Role) role : null;
	}
}