package wauj.web.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionUserDao;
import wauj.web.model.ElectionUser;

public class ElectionUserDaoImpl implements IElectionUserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private static String getElectionUserList = "SELECT election_user_id AS electionUserId, election_id AS electionId, user_id AS userId, voted FROM election_user";
	private static String getElectionUserById = "SELECT election_user_id AS electionUserId, election_id AS electionId, user_id AS userId, voted FROM election_user WHERE election_user_id = ?";
	private static String getElectionUserByFKs = "SELECT election_user_id AS electionUserId, election_id AS electionId, user_id AS userId, voted FROM election_user WHERE user_id = ? AND election_id = ?";

	@Override
	public void setVoted(int electionUserId, Date vote) {
		Session session = this.sessionFactory.getCurrentSession();      
		ElectionUser electionUser = (ElectionUser) session.get(ElectionUser.class, new Integer(electionUserId));
		
		electionUser.setVoted(vote);

        session.update(electionUser);
	}

	@Override
	public ElectionUser getElectionUserByFks(int userId, int electionId) {
		Object electionUser = sessionFactory.getCurrentSession()
				.createSQLQuery(getElectionUserByFKs)
				.setResultTransformer(Transformers.aliasToBean(ElectionUser.class))
				.setParameter(0, userId)
				.setParameter(1, electionId)
				.uniqueResult();
		
		return electionUser != null ? (ElectionUser)electionUser : null;
	}

	@Override
	public ElectionUser get(int electionUserId) {
		Object electionUser = sessionFactory.getCurrentSession()
				.createSQLQuery(getElectionUserById)
				.setResultTransformer(Transformers.aliasToBean(ElectionUser.class))
				.setParameter(0, electionUserId)
				.uniqueResult();
		
		return electionUser != null ? (ElectionUser)electionUser : null;
	}

	@Override
	public void save(ElectionUser electionUser) {
		this.sessionFactory.getCurrentSession().save(electionUser);
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	@Override
	public List<ElectionUser> getUserList(int electionId) {
		List<ElectionUser> electionUserList = sessionFactory
				.getCurrentSession()
				.createSQLQuery(getElectionUserList + " WHERE election_id = " + electionId)
				.setResultTransformer(Transformers.aliasToBean(ElectionUser.class))
				.list();
		
		return electionUserList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clearUsers(int electionId) {
		List<ElectionUser> electionList = sessionFactory
				.getCurrentSession()
	    		.createSQLQuery(getElectionUserList + " WHERE election_id = " + electionId)
				.setResultTransformer(Transformers.aliasToBean(ElectionUser.class))
				.list();
		
	    for (ElectionUser electionUser : electionList)
	    {
	    	this.sessionFactory.getCurrentSession().delete(electionUser);
	    }
	}
}
