package wauj.web.dao.hibernate;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.ILoginDao;

public class LoginDaoImpl implements ILoginDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	private static String getUserPassword = "SELECT password FROM user WHERE is_confirmed  = 1 AND email = ?";

	@Transactional(readOnly = true)
	@Override
	public boolean isPasswordCorrect(String email, String password) {
		String passwordFromDB = (String) sessionFactory.getCurrentSession().createSQLQuery(getUserPassword).setParameter(0, email).uniqueResult();

		if (passwordFromDB == null || passwordFromDB.equals("")) {
			return false;
		}

		return bCryptPasswordEncoder.matches(password, passwordFromDB);
	}
}