package wauj.web.dao;

import java.util.Date;
import java.util.List;

import wauj.web.model.ElectionUser;


public interface IElectionUserDao {
	public void setVoted(int electionUserId, Date vote);
	public ElectionUser getElectionUserByFks(int userId, int electionId);
	public ElectionUser get(int electionUserId);
	public void save(ElectionUser electionUser);
	public List<ElectionUser> getUserList(int electionId);;
	public void clearUsers(int electionId);
}
