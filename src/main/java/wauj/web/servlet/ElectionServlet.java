package wauj.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.databind.ObjectMapper;

import wauj.web.security.ElectionOption;
import wauj.web.spring.service.ElectionOptionService;

public class ElectionServlet extends HttpServlet{

	private static final long serialVersionUID = -5813097670064195079L;
	
	@Autowired
	private ElectionOptionService electionOptionService;
	
	@Override
	 public void init(final ServletConfig config) throws ServletException {
	  super.init(config);
	  SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
	 }
	
	protected void doPost(HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		
		Integer electionId = Integer.parseInt((request.getParameter("electionId")));
		List<ElectionOption> listOptions = this.electionOptionService.listElectionOptions(electionId);

		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(listOptions) ;
		
		response.setContentType("application/json");
		PrintWriter writer = response.getWriter();        
        writer.println(json);
	}
}
