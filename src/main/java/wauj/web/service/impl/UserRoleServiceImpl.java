package wauj.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IUserRoleDao;
import wauj.web.model.UserRole;
import wauj.web.service.IUserRoleService;

public class UserRoleServiceImpl implements IUserRoleService {
	
	@Autowired
    private IUserRoleDao userRoleDao;

    @Override
    @Transactional
	public void addUserRole(UserRole userRole){
        this.userRoleDao.addUserRole(userRole);
	}
}
