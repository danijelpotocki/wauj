package wauj.web.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
 
@Service("mailService")
public class ApplicationMailer
{
     
    @Autowired
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    
    @Autowired
    private SimpleMailMessage preConfiguredMessage;
 
    /**
     * This method will send compose and send the message
     * */
    public void sendMail(String to, String subject, String body)
    {

    	try {
        	MimeMessage message = mailSender.createMimeMessage();

        	// use the true flag to indicate you need a multipart message
        	MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        	
        	helper.setTo(to);
        	helper.setSubject(subject);
			helper.setText(body, body);

	    	mailSender.send(message);

		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    /**
     * This method will send a pre-configured message
     * */
    public void sendPreConfiguredMail(String message)
    {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
    
    public String getFile(String fileName) {
    	 
    	StringBuilder result = new StringBuilder("");
     
    	//Get file from resources folder
    	ClassLoader classLoader = getClass().getClassLoader();
    	File file = new File(classLoader.getResource(fileName).getFile());
    	
    	InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);
     
    	BufferedReader in = null;
		try {
			in = new BufferedReader(
					new InputStreamReader( is, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(in != null) {
			String str;
			try {
				while ((str = in.readLine()) != null) {
					result.append(str).append("\n");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	     
	    	return result.toString();
		}
		
		return "";

      }
}