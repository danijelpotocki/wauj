package wauj.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IRoleDao;
import wauj.web.dao.IUserRoleDao;
import wauj.web.model.UserRole;
import wauj.web.security.Role;
import wauj.web.security.User;
import wauj.web.service.IRoleService;

public class RoleServiceImpl implements IRoleService {
	
	@Autowired
    public IRoleDao roleDao;
	
	@Autowired
    public IUserRoleDao userRoleDao;
	
    @Override
    @Transactional
	public Role getRoleByTitle(String roleTitle){
    	return roleDao.getRoleByTitle(roleTitle);
	}
    
    @Override
    @Transactional
    public void addRoleUser(User user, String roleTitle) {
		
    	Role role = this.getRoleByTitle(roleTitle);
    	UserRole userRole = new UserRole(role.getRoleId(), user.getUserId());
    	userRoleDao.addUserRole(userRole);
	}
}
