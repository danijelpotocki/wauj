package wauj.web.service;

import wauj.web.security.Role;
import wauj.web.security.User;

public interface IRoleService {

	Role getRoleByTitle(String roleTitle);

	void addRoleUser(User user, String roleTitle);
}
