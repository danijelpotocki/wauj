package wauj.web.service;

import wauj.web.model.UserRole;

public interface IUserRoleService {

	void addUserRole(UserRole userRole);

}
