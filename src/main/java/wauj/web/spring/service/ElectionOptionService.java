package wauj.web.spring.service;

import java.util.List;

import wauj.web.security.ElectionOption;

public interface ElectionOptionService {
	public List<ElectionOption> addElectionOption(ElectionOption o);
	public List<ElectionOption> deleteElectionOption(Integer electionOptionId);
	public List<ElectionOption> listElectionOptions(Integer electionId);
	public List<ElectionOption> listElectionOptionsFor(Integer electionID);
	public void addVote(Integer electionOptionId);
	public ElectionOption get(Integer electionOptionId);
}
