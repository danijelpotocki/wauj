package wauj.web.spring.service;

import java.util.List;

import wauj.web.security.Election;
import wauj.web.security.FinishedElection;

public interface ElectionService {

	public void addElection(Election p);
	public void updateElection(Election p);
	public List<Election> listElections();
	public List<Election> listActiveElections();
	public List<FinishedElection> listFinishedElections();
	public void removeElection(int id);
	public Election getElection(int id);
}
