package wauj.web.spring.service;

import java.util.Date;
import java.util.List;

import wauj.web.model.ElectionUser;

public interface ElectionUserService {
	public void vote(Integer electionUserId, Date vote);
	public ElectionUser getElectionUserByFks(Integer userId, Integer electionId);
	public ElectionUser get(Integer electionUserId);
	public void addElectionUser(ElectionUser electionUser);
	public List<ElectionUser> getUserList(Integer electionId);
	public void clearUsers(Integer electionId);
}
