package wauj.web.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionOptionDao;
import wauj.web.security.ElectionOption;

@Service
public class ElectionOptionServiceImpl implements ElectionOptionService{

	@Autowired
	private IElectionOptionDao electionOptionDao;
	
	@Transactional
	public List<ElectionOption> addElectionOption(ElectionOption o) {
		return this.electionOptionDao.add(o);
	}

	@Transactional
	public List<ElectionOption> deleteElectionOption(Integer electionOptionId) {
		return this.electionOptionDao.delete(electionOptionId);
	}

	@Transactional
	public List<ElectionOption> listElectionOptions(Integer electionId) {
		return this.electionOptionDao.list(electionId);
	}
	
	@Transactional
	public List<ElectionOption> listElectionOptionsFor(Integer electionId) {
		return this.electionOptionDao.listOptionsFor(electionId);
	}

	@Transactional
	public void addVote(Integer electionOptionId) {
		this.electionOptionDao.addVote(electionOptionId);
		return;
	}

	@Transactional
	public ElectionOption get(Integer electionOptionId) {
		return this.electionOptionDao.get(electionOptionId);
	}
}
