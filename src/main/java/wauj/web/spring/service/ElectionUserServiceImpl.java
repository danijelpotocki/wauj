package wauj.web.spring.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionUserDao;
import wauj.web.model.ElectionUser;

public class ElectionUserServiceImpl implements ElectionUserService {

	@Autowired
	private IElectionUserDao electionUserDao;
	
	@Transactional
	public void vote(Integer electionUserId, Date vote) {
		this.electionUserDao.setVoted(electionUserId, vote);
		return;
	}

	@Transactional
	public ElectionUser getElectionUserByFks(Integer userId, Integer electionId) {
		return this.electionUserDao.getElectionUserByFks(userId, electionId);
	}

	@Transactional
	public ElectionUser get(Integer electionUserId) {
		return this.electionUserDao.get(electionUserId);
	}

	@Transactional
	public void addElectionUser(ElectionUser electionUser) {
		this.electionUserDao.save(electionUser);
		return;
	}

	@Transactional
	public List<ElectionUser> getUserList(Integer electionId) {
		return this.electionUserDao.getUserList(electionId);
	}

	@Transactional
	public void clearUsers(Integer electionId) {
		this.electionUserDao.clearUsers(electionId);
		return;
	}
}
