package wauj.web.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wauj.web.dao.IElectionDao;
import wauj.web.security.Election;
import wauj.web.security.FinishedElection;

@Service
public class ElectionServiceImpl implements ElectionService {

	@Autowired
	private IElectionDao electionDao;
	
	public void setPersonDAO(IElectionDao electionDao) {
        this.electionDao = electionDao;
    }
	
    @Transactional
	public void addElection(Election election) {
    	this.electionDao.save(election);
	}

    @Transactional
	public void updateElection(Election election) {
		this.electionDao.update(election);
	}

	@Transactional
	public List<Election> listElections() {
		return this.electionDao.list();
	}
	
	@Transactional
	public List<Election> listActiveElections() {
		return this.electionDao.listActive();
	}
	
	@Transactional
	public List<FinishedElection> listFinishedElections() {
		return this.electionDao.listFinished();
	}

    @Transactional
	public void removeElection(int id) {
		this.electionDao.delete(id);
	}
    
    @Transactional
    public Election getElection (int id) {
    	return this.electionDao.get(id);
    }
}
