package wauj.web.model;

/**
 * Enum for roles. All possible roles should be put in this class as constants.
 * @author Danijel
 *
 */
public enum Roles {
	ADMIN("ROLE_ADMIN"),
	USER("ROLE_USER");

	private final String name;

	private Roles(final String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
