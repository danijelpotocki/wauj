package wauj.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;


public class ElectionUser implements Serializable {
	private static final long serialVersionUID = -13425451276459789L;
	
	private Integer electionUserId;
	
	@NotNull
	private Integer electionId;
	
	@NotNull
	private Integer userId;

	private Date voted;

	public ElectionUser() {
		super();
	}
	
	public ElectionUser(Integer electionUserId, Integer electionId, Integer userId,
			Date voted) {
		super();
		this.electionUserId = electionUserId;
		this.electionId = electionId;
		this.userId = userId;
		this.voted = voted;
	}

	public Integer getElectionUserId() {
		return electionUserId;
	}

	public void setElectionUserId(Integer title) {
		this.electionUserId = title;
	}

	public Integer getElectionId() {
		return electionId;
	}

	public void setElectionId(Integer electionId) {
		this.electionId = electionId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getVoted() {
		return voted;
	}

	public void setVoted(Date voted) {
		this.voted = voted;
	}
}