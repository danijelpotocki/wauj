package wauj.web.model;

public class UserRole {

	private Integer userRoleId;
	private Integer roleId;
	private Integer userId;
	
	public UserRole(Integer userRoleId, Integer roleId, Integer userId) {
		super();
		this.userRoleId = userRoleId;
		this.roleId = roleId;
		this.userId = userId;
	}
	
	public UserRole(Integer roleId, Integer userId) {
		super();
		this.roleId = roleId;
		this.userId = userId;
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
