package wauj.web.security;

import java.io.Serializable;

public class ElectionOption implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1495742286553903061L;
	private Integer electionOptionId;
	private String title;
	private Integer votes;
	private Integer electionId;
	
	public ElectionOption() {
		super();
		this.votes = 0;
	}
	
	public ElectionOption(Integer electionOptionId, String title, Integer electionId) {
		super();
		
		this.electionOptionId = electionOptionId;
		this.title = title;
		this.electionId = electionId;
		this.votes = 0;
	}
	
	public Integer getElectionOptionId() {
		return electionOptionId;
	}

	public void setElectionOptionId(Integer electionOptionId) {
		this.electionOptionId = electionOptionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getVotes() {
		return votes;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}

	public Integer getElectionId() {
		return electionId;
	}

	public void setElectionId(Integer electionId) {
		this.electionId = electionId;
	}

	
	
}
