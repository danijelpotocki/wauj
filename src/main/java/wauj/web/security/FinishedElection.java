package wauj.web.security;

import java.util.Date;

public class FinishedElection {
	private Integer electionId;
	private String title;
	private String description;
	private Date startDate;
	private Date endDate;
	private Integer electionOptionId;
	private String electionOptionTitle;
	private Integer votes;
	
	public FinishedElection() {
		super();
		this.setVotes(0);
	}
	
	public FinishedElection(Integer electionId, String title, String description,
			Date startDate, Date endDate,Integer electionOptionId, String electionOptionTitle) {
		super();
		this.setElectionId(electionId);
		this.setTitle(title);
		this.setDescription(description);
		this.setStartDate(startDate);
		this.setEndDate(endDate);		
		this.setElectionOptionId(electionOptionId);
		this.setElectionOptionTitle(title);
		this.setElectionId(electionId);
		this.setVotes(0);
	}

	public Integer getElectionId() {
		return electionId;
	}

	public void setElectionId(Integer electionId) {
		this.electionId = electionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getElectionOptionId() {
		return electionOptionId;
	}

	public void setElectionOptionId(Integer electionOptionId) {
		this.electionOptionId = electionOptionId;
	}

	public String getElectionOptionTitle() {
		return electionOptionTitle;
	}

	public void setElectionOptionTitle(String electionOptionTitle) {
		this.electionOptionTitle = electionOptionTitle;
	}

	public Integer getVotes() {
		return votes;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}
	
}
