package wauj.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import wauj.web.dao.IRoleDao;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private IRoleDao roleDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<GrantedAuthority> authorities = new ArrayList<>();
		Role userRole = roleDao.getRoleForUser(username);

		if (userRole != null) {
			authorities.add(new SimpleGrantedAuthority(userRole.getTitle()));
		}

		UserDetails result = new User(username, "EMPTY_PASS", authorities);
		return result;
	}
}