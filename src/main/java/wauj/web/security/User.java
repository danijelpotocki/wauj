package wauj.web.security;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class User implements Serializable {
	private static final long serialVersionUID = -8791231616243923463L;

	private Integer userId;
	
	@NotEmpty
	@Email
	private String email;
	
	@NotEmpty
	private String name;

	@NotEmpty
	private String surname;

	@NotEmpty
	@Length(min=11, max=11)
	private String oib;
	private String address;
	
	private String password;
	private String uniqueCode;
	private String confirmation;
	private Integer isConfirmed;

	public User() {}

	public User(Integer userId, String email, String name, String surname,
			String oib, String address, String password, String uniqueCode,
			String confirmation, Integer isConfirmed) {
		super();
		this.userId = userId;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.oib = oib;
		this.address = address;
		this.password = password;
		this.uniqueCode = uniqueCode;
		this.confirmation = confirmation;
		this.isConfirmed = isConfirmed;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOib() {
		return oib;
	}

	public void setOib(String oib) {
		this.oib = oib;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	public Integer getIsConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(Integer isConfirmed) {
		this.isConfirmed = isConfirmed;
	}
}