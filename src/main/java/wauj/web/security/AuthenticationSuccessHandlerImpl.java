package wauj.web.security;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import wauj.web.dao.IUserDao;

public class AuthenticationSuccessHandlerImpl extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private IUserDao userDao;

	private static final Logger _log = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		_log.debug("User authenticated successfully.");

		// get current logged in user
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userDao.get(auth.getName());

		// store data to session
		_log.debug("Storing session attribute {} to {}", SessionObjects.USER, currentUser.getUserId());
		currentUser.setPassword("*");	// don't allow password to get to the client
		request.getSession().setAttribute(SessionObjects.USER, currentUser);

		setDefaultTargetUrl("/index");
		super.onAuthenticationSuccess(request, response, authentication);
	}
}