package wauj.web.security;

import java.util.List;

public interface UserService {
	public void addUser(User user);
	void updateUser(User user);
	User getUserById(int id);
	void deleteUser(int id);
	List<User> listUsers();
	User getCurrentUser();
	Boolean confirmUser(String code);
	public void forgotPassword(String email);
}
