package wauj.web.security;

import org.springframework.beans.factory.annotation.Autowired;

import wauj.web.dao.ILoginDao;

public class UserLoginService {

	@Autowired
	private ILoginDao loginDao;

	public boolean isPasswordCorrect(String username, String password) {
		return loginDao.isPasswordCorrect(username, password);
	}
}