package wauj.web.security;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.lang.RandomStringUtils;

import wauj.web.dao.IUserDao;
import wauj.web.model.Roles;
import wauj.web.service.IRoleService;
import wauj.web.service.impl.ApplicationMailer;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    private IUserDao userDao;
	
	@Autowired
    private IRoleService roleService;
	
    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }
    
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Override
    @Transactional
    public User getUserById(int id) {
        return this.userDao.getUserById(id);
    }
    
    @Override
    @Transactional
	public void addUser(User user){
    	
    	String pass = user.getPassword();
    	
		user.setPassword(bCryptPasswordEncoder.encode(pass));
		user.setIsConfirmed(0);
		
		String confirmationCode = UUID.randomUUID().toString();
		String uniqueCode = RandomStringUtils.randomAlphanumeric(20);
		
		user.setConfirmation(confirmationCode);
		user.setUniqueCode(uniqueCode);
		
        this.userDao.addUser(user);
        this.roleService.addRoleUser(user, Roles.USER.toString());
        this.sendConfirmationMail(user);
	}

    @Override
    @Transactional
    public void updateUser(User user) {
    	
    	User oldUser = this.getUserById(user.getUserId());
    	
    	Boolean emailChanged = false;
    	String oldEmail = oldUser.getEmail();
    	if(oldEmail.compareTo(user.getEmail()) != 0) {
    		oldUser.setIsConfirmed(0);
    		emailChanged = true;
    	}
    	
    	String pass = user.getPassword();
    	if(pass.length() != 0) {
    		oldUser.setPassword(bCryptPasswordEncoder.encode(pass));
    	}
    	
    	oldUser.setName(user.getName());
    	oldUser.setSurname(user.getSurname());
    	oldUser.setAddress(user.getAddress());
    	oldUser.setEmail(user.getEmail());
    	oldUser.setOib(user.getOib());
    	
        this.userDao.updateUser(oldUser);
        
        if(emailChanged == true) {
        	this.sendConfirmationMail(oldUser);
        }
    }
    
    @Override
    @Transactional
    public void deleteUser(int id) {
        this.userDao.deleteUser(id);
    }
    
    @Override
    @Transactional
    public List<User> listUsers() {
        return this.userDao.listUser();
    }
    
    @Override
    @Transactional
    public User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String email = auth.getName();
	    User user = this.userDao.get(email);
	    
	    return user;
    }
    
    @Override
    @Transactional
    public Boolean confirmUser(String code) {
    	
        User user = this.userDao.getUserByCode(code);
        
        if(user == null) {
        	return false;
        }
        
        user.setIsConfirmed(1);
        this.userDao.updateUser(user);
    	this.sendWelcomeMail(user);
        
    	return true;
    }
    
    @Override
    @Transactional
    public void forgotPassword(String email) {
    	
    	User user = this.userDao.get(email);
    	
    	String newPassword = RandomStringUtils.randomAlphanumeric(20);
    	user.setPassword(bCryptPasswordEncoder.encode(newPassword));
    	
    	this.userDao.updateUser(user);
    	this.sendForgotPasswordMail(user, newPassword);
    }
    
    public void sendConfirmationMail(User user) {
        ApplicationContext context = new FileSystemXmlApplicationContext("mail-config.xml");
        ApplicationMailer mailer = (ApplicationMailer) context.getBean("mailService");
        
        String mailBody = mailer.getFile("mail/confirm.html");
        mailBody = mailBody.replace("{confirmLink}", "http://localhost:8080/wauj/user/confirm/" + user.getConfirmation());
        mailBody = mailBody.replace("{confirmPage}", "http://localhost:8080/wauj/user/confirmpage");
		mailBody = mailBody.replace("{confirmCode}", user.getConfirmation());
        		
        mailer.sendMail(user.getEmail() , "Confirmation mail", mailBody);
	}
    
    public void sendWelcomeMail(User user) {
        ApplicationContext context = new FileSystemXmlApplicationContext("mail-config.xml");
        ApplicationMailer mailer = (ApplicationMailer) context.getBean("mailService");
        
        String mailBody = mailer.getFile("mail/welcome.html");
        mailBody = mailBody.replace("{name}", user.getName());
        mailBody = mailBody.replace("{uniqueCode}", user.getUniqueCode());
        		
        mailer.sendMail(user.getEmail() , "Welcome mail", mailBody);
	}
    
    public void sendForgotPasswordMail(User user, String newPassword) {
        ApplicationContext context = new FileSystemXmlApplicationContext("mail-config.xml");
        ApplicationMailer mailer = (ApplicationMailer) context.getBean("mailService");
        
        String mailBody = mailer.getFile("mail/forgot.html");
        mailBody = mailBody.replace("{name}", user.getName());
        mailBody = mailBody.replace("{password}", newPassword);
        		
        mailer.sendMail(user.getEmail() , "Forgot password mail", mailBody);
	}
}
