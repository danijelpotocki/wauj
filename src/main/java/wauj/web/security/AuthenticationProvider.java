package wauj.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UserLoginService userLoginService;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		if (authentication.getName().isEmpty()) {
			throw new BadCredentialsException("Username is not entered!");
		}

		String password = (String) authentication.getCredentials();
		if (password == null || password.isEmpty()) {
			throw new BadCredentialsException("Password is not entered!");
		}

		if (!userLoginService.isPasswordCorrect(authentication.getName(), password)) {
			throw new BadCredentialsException("Username or password is not correct, or you didn't confirm your account!");
		}
	}

	@Override
	protected UserDetails retrieveUser(String username,	UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		UserDetails userDetails = null;

		try {
			userDetails = userDetailsService.loadUserByUsername(username);
		} catch (UsernameNotFoundException  ex) {
			throw new AuthenticationServiceException("User could not be loaded!");
		}

		if (userDetails == null) {
			throw new AuthenticationServiceException("User service didn't return required user!");
		}

		return userDetails;
	}
}