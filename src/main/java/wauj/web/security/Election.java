package wauj.web.security;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class Election implements Serializable {
	private static final long serialVersionUID = -403250971215465050L;
	
	private Integer electionId;
	
	@NotEmpty
	private String title;
	
	private String description;

	@NotNull
	private Date startDate;

	@NotNull
	private Date endDate;
	
	public Election() {
		super();
	}
	
	public Election(Integer electionId, String title, String description,
			Date startDate, Date endDate) {
		super();
		this.electionId = electionId;
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getElectionId() {
		return electionId;
	}

	public void setElectionId(Integer electionId) {
		this.electionId = electionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
