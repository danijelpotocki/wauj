package wauj.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	private static final Logger _log = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping( value = { "/index", "/" }, method = { RequestMethod.GET, RequestMethod.POST } )
	public ModelAndView redirectToHome() {

		ModelAndView mav = new ModelAndView("index");

		return mav;
	}

	// Example for sending object to view
	/*
	 * @RequestMapping(value="/userData") public ModelAndView
	 * getUserData(HttpServletRequest request, HttpServletResponse response) {
	 * UserData userData = userDataDao.getUserData(1);
	 * 
	 * ModelAndView mav = new ModelAndView("userData/viewUserData");
	 * mav.addObject("userDataAttr", userData);
	 * 
	 * _log.trace("Returning user data: <" + userData + ">");
	 * 
	 * return mav; }
	 */
}
