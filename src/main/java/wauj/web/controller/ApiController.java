package wauj.web.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import wauj.web.security.ElectionOption;
import wauj.web.spring.service.ElectionOptionService;

@RestController
public class ApiController {

	@Autowired
	private ElectionOptionService electionOptionService;

	// List<ElectionOption> listOptions - returns array of arrays :D
	// 0 - electionOptionID
	// 1 - Title
	// 2 - parent
	// 3 - voteZ

	@RequestMapping("/api/elections/options/add")
	public List<ElectionOption> addOption(final HttpServletRequest request, @RequestParam(value = "electionId", required = true) Integer electionId, @RequestParam(value = "optionTitle",
			required = true) String optionTitle) {
		ElectionOption option = new ElectionOption();
		option.setElectionId(electionId);
		String title = "";
		try {
			title = new String(request.getParameter("optionTitle").getBytes("ISO-8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		option.setTitle(title);

		List<ElectionOption> listOptions = this.electionOptionService.addElectionOption(option);

		return listOptions;
	}

	@RequestMapping("/api/elections/options/deleteOption")
	public List<ElectionOption> deleteOption(@RequestParam(value = "electionOptionId", required = true) Integer electionOptionId) {

		List<ElectionOption> listOptions = this.electionOptionService.deleteElectionOption(electionOptionId);

		return listOptions;
	}

	@RequestMapping("/api/elections/options/getAll")
	public List<ElectionOption> getAllOptions(@RequestParam(value = "electionId", required = true) Integer electionId) {

		List<ElectionOption> listOptions = this.electionOptionService.listElectionOptions(electionId);

		return listOptions;
	}

	//@RequestMapping("/api/elections/graph")
	public List<ElectionOption> getGraph(@RequestParam(value = "electionId", required = true) Integer electionId) {

		List<ElectionOption> listOptions = this.electionOptionService.listElectionOptions(electionId);

		return listOptions;
	}

}
