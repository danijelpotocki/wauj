package wauj.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import wauj.web.model.Roles;
import wauj.web.security.User;
import wauj.web.security.UserService;
import wauj.web.utils.SecurityUtils;

@Controller
public class AccountController {
	
	private static final Logger _log = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = { "/user/login" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView redirectToLogin(final HttpServletRequest request, final HttpServletResponse response) {
		_log.debug("Redirecting user to login page.");

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated()) {
			_log.debug("User IS authenticated");
			return new ModelAndView("redirect:/index");
		}
		else {
			_log.debug("User IS NOT authenticated");
			ModelAndView mav = new ModelAndView("index");
			mav.addObject("partial", "login");

			return mav;
		}
	}
	
	@RequestMapping( value = { "/user/register" }, method = { RequestMethod.GET} )
	public ModelAndView redirectToRegister(final HttpServletRequest request, final HttpServletResponse response) {
		_log.debug("Redirecting user to register page.");

		User user = new User();
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial","user/edit");
		mav.addObject("userBean", user);

		return mav;
	}
	
	@RequestMapping(value= "/user/register", method = { RequestMethod.POST})
	public ModelAndView postRegister(final HttpServletRequest request, @ModelAttribute("userBean") @Valid User user, BindingResult result) {
		ModelAndView mav = new ModelAndView("index");
		
        if(result.hasErrors()) {        	
    		mav.addObject("partial","user/edit");
        	return mav;
        }
		
		String pass = user.getPassword();
		String passConfirm = request.getParameter("password_confirm");

		if (!pass.equals(passConfirm)) {
			mav.addObject("passError", "Potvrda lozinke mora biti jednaka lozinci.");
			mav.addObject("partial","user/edit");
        	return mav;
		}

		try {
			user.getUserId();
			userService.updateUser(user);
		} catch (Exception e) {
			userService.addUser(user);
		}

		_log.debug("Went through register post.");

		return new ModelAndView("redirect:/index");

	}
    
    @RequestMapping( value = {"/user/details/{id}"} , method = { RequestMethod.GET} )
    public ModelAndView detailsUser(@PathVariable("id") int id){
		return doDetailsUser(id, "details");
    }
    
    @RequestMapping( value = {"/user/details", "/user/account"} , method = { RequestMethod.GET} )
    public ModelAndView detailsCurrentUser(){
		return doDetailsUser(0, "details");
    }
    
    @RequestMapping( value = {"/user/edit/{id}"} , method = { RequestMethod.GET} )
    public ModelAndView editUser(@PathVariable("id") int id){
		return doDetailsUser(id, "edit");
    }
    
    @RequestMapping( value = {"/user/edit"} , method = { RequestMethod.GET} )
    public ModelAndView editCurrentUser(){
		return doDetailsUser(0, "edit");
    }
    
    public ModelAndView doDetailsUser(int id, String path){
    	  
    	User user = new User();
    	User currentUser = this.userService.getCurrentUser();
    	    	
    	if(id != 0 && (id == currentUser.getUserId() || SecurityUtils.userHasRole(Roles.ADMIN) )) {
    		user = this.userService.getUserById(id);
    	} else {
    		user = currentUser;
    	}
    	
    	ModelAndView mav = new ModelAndView("index");
    	mav.addObject("userBean", user);
		mav.addObject("partial","user/" + path);

		return mav;
    }
    
    @RequestMapping( value = "/user/remove/{id}", method = { RequestMethod.GET})
    public ModelAndView deleteUser(@PathVariable("id") int id){
         
        this.userService.deleteUser(id);
		return new ModelAndView("redirect:/user/list");

    }
    
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public ModelAndView listUsers(Model model) {
    	
    	ModelAndView mav = new ModelAndView("index");
    	mav.addObject("userList", this.userService.listUsers());
		mav.addObject("partial","user/list");

        return mav;
    }
    
	
	@RequestMapping(value = { "/user/confirm/{code}" }, method = { RequestMethod.GET})
	public ModelAndView confirmUser(@PathVariable("code") String code) {
		return this.doConfirm(code);
	}
	
	@RequestMapping(value = { "/user/confirm" }, method = { RequestMethod.POST})
	public ModelAndView postConfirmUser(final HttpServletRequest request, final HttpServletResponse response) {
		String code = request.getParameter("code");
		return this.doConfirm(code);
	}
	
	protected ModelAndView doConfirm(String code) {
		Boolean confirmed = this.userService.confirmUser(code);
		
		String success = "danger";
		String message = "";
		if(confirmed == true) {
			message = "Vaš račun je uspješno potvrđen";
			success = "success";
			
		} else {
			message = "Vaš kod nije točan";
		}
		
		ModelAndView mav = new ModelAndView("index");
    	mav.addObject("success", success);
    	mav.addObject("message", message);
		mav.addObject("partial","user/alert");
    	
		return mav;
	}
	
	@RequestMapping(value = { "/user/confirmpage" }, method = { RequestMethod.GET})
	public ModelAndView redirectConfirmPage() {

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial","user/confirmpage");
    	
		return mav;
	}
	
	@RequestMapping(value = { "/user/forgot" }, method = { RequestMethod.GET})
	public ModelAndView redirectForgotPassword() {

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial","user/forgot");
    	
		return mav;
	}
	
	@RequestMapping(value = { "/user/forgot" }, method = { RequestMethod.POST})
	public ModelAndView postForgotPassword(final HttpServletRequest request) {
		
		String email = request.getParameter("email");

		this.userService.forgotPassword(email);
		
		ModelAndView mav = new ModelAndView("index");
    	mav.addObject("success", "success");
    	mav.addObject("message", "Nova lozinka je poslana na vaš e-mail.");
		mav.addObject("partial","user/alert");
    	
		return mav;
	}
}
