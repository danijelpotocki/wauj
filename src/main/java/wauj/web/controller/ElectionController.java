package wauj.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import wauj.web.model.ElectionUser;
import wauj.web.security.Election;
import wauj.web.security.ElectionOption;
import wauj.web.security.UserService;
import wauj.web.security.FinishedElection;
import wauj.web.spring.service.ElectionOptionService;
import wauj.web.spring.service.ElectionService;
import wauj.web.spring.service.ElectionUserService;

@Controller
public class ElectionController {

	@Autowired
	private ElectionService electionService;
	@Autowired
	private ElectionOptionService electionOptionService;
	@Autowired
	private ElectionUserService electionUserService;
	@Autowired
	private UserService userService;
	
	private static final Logger _log = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = { "/elections" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView listElection(HttpServletRequest request, HttpServletResponse response) {
		List<Election> listElections = electionService.listElections();
		
		ModelAndView mav = new ModelAndView("index", "electionList", listElections);
		mav.addObject("partial", "elections");

		return mav;
	}

	@RequestMapping(value = "/elections/postElection", method = RequestMethod.POST)
	public ModelAndView postElection(final HttpServletRequest request, @ModelAttribute("electionBean") @Valid Election election, BindingResult result) {

        if(result.hasErrors()) {
        	ModelAndView mav = new ModelAndView("index");
        	mav.addObject("partial", "electionDetails");
        	return mav;
        }
        
		this.electionService.addElection(election);

		_log.debug("Went through election post.");

		return new ModelAndView("redirect:/electionOptions/" + election.getElectionId());

	}

	@RequestMapping(value = "/elections/update/postElection", method = RequestMethod.POST)
	public ModelAndView postUpdateElection(final HttpServletRequest request, @ModelAttribute("electionBean") @Valid Election election, BindingResult result) {

        if(result.hasErrors()) {
        	ModelAndView mav = new ModelAndView("index");
        	mav.addObject("partial", "electionDetails");
            return mav;
        }
        
		this.electionService.updateElection(election);

		_log.debug("Went through election post.");

		return new ModelAndView("redirect:/electionOptions/" + election.getElectionId());

	}

	@RequestMapping(value = { "/elections/add" }, method = RequestMethod.GET)
	public ModelAndView addElection(final HttpServletRequest request, final HttpServletResponse response) {

		Election election = new Election();
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial", "electionDetails");
		mav.addObject("electionBean", election);

		return mav;
	}

	@RequestMapping(value = { "/elections/remove" }, method = RequestMethod.GET)
	public ModelAndView removeElection(HttpServletRequest request, HttpServletResponse response) {

		if (electionService.getElection(Integer.parseInt(request.getParameter("id"))).getStartDate().compareTo(new Date()) <= 0)
		{
			return new ModelAndView("redirect:/elections");
		}
		
		this.electionService.removeElection(Integer.parseInt(request.getParameter("id")));

		_log.debug("Went through election deletion post.");

		return new ModelAndView("redirect:/elections");
	}

	@RequestMapping(value = { "/elections/update/{id}" }, method = RequestMethod.GET)
	public ModelAndView editElection(@PathVariable("id") int id) {

		if (electionService.getElection(id).getStartDate().compareTo(new Date()) <= 0)
		{
			return new ModelAndView("redirect:/elections");
		}

		Election election = this.electionService.getElection(id);
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial", "electionDetails");
		mav.addObject("electionBean", election);

		return mav;
	}

	@RequestMapping(value = { "/electionOptions/{id}" }, method = RequestMethod.GET)
	public ModelAndView editElectionOptions(@PathVariable("id") Integer id) {

		/*if (electionService.getElection(id).getStartDate().compareTo(new Date()) <= 0)
		{
			return new ModelAndView("redirect:/elections");
		}*/
		
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("id", id.toString());
		mav.addObject("partial", "electionOptions");
		
		return mav;
	}

	@RequestMapping(value = { "/elections/active" } , method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView getActiveElections() {
		List<Election> activeElectionsList = electionService.listActiveElections();
		
		ModelAndView mav = new ModelAndView("index", "activeElectionsList", activeElectionsList);
		mav.addObject("partial", "electionsActive");

		return mav;
	}
	
	@RequestMapping(value = { "/elections/voting/{id}" } , method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView getActiveElections(@PathVariable("id") int electionID) {
		List<ElectionOption> electionOptionsList = electionOptionService.listElectionOptionsFor(electionID);

		ElectionUser electionUser = 
			electionUserService.getElectionUserByFks(
				userService.getCurrentUser().getUserId(), 
				electionID);
		
		Boolean voteable = electionUser != null;
		
		if (voteable)
		{
			voteable = electionUser.getVoted() == null;
		}
		
		ModelAndView mav = new ModelAndView("index", "electionOptionsList", electionOptionsList);
		mav.addObject("partial", "voting");
		mav.addObject("voteable", voteable);

		return mav;
	}
	
	@RequestMapping(value = { "/elections/voting/addVote/{id}" } , method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView vote(@PathVariable("id") int electionOptionID) {
		
		electionOptionService.addVote(electionOptionID);
		electionUserService.vote(
				electionUserService.getElectionUserByFks(userService.getCurrentUser().getUserId(), 
				electionOptionService.get(electionOptionID).getElectionId()).getElectionUserId(), new Date());
		
		return new ModelAndView("redirect:/elections/voting/voteConfirmed");
	}
	
	@RequestMapping(value = { "/elections/voting/voteConfirmed" } , method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView voteConfirmed() {
		
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial", "voteConfirmed");
		
		return mav;
	}
	
	// Results
	@RequestMapping(value = { "/results" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView results() {
		List<FinishedElection> electionsList = electionService.listFinishedElections(); 
		
		ModelAndView mav =  new ModelAndView("index", "electionsList", electionsList);
		mav.addObject("partial", "results");

		return mav;
	}
	
	@RequestMapping(value = { "/results/details/{id}" }, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView resultDetails(@PathVariable("id") int electionID) {
		Election election = this.electionService.getElection(electionID);
		
		int prvi = 45;
		int drugi = 55;

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("partial", "resultDetails");
		mav.addObject("columns", "[['prvi', " + prvi + "],['drugi', " + drugi + "],]");
		mav.addObject("electionBean", election);
		return mav;
	}


	@RequestMapping(value = { "/electionUsers/{id}" }, method = RequestMethod.GET)
	public ModelAndView editElectionUsers(@PathVariable("id") Integer id) {

		/*if (electionService.getElection(id).getStartDate().compareTo(new Date()) <= 0)
		{
			return new ModelAndView("redirect:/elections");
		}*/
		
		List<ElectionUser> existingElectionUsers = electionUserService.getUserList(id);
		
		List<Integer> selectedUsers = new ArrayList<Integer>();
		
		for (ElectionUser user : existingElectionUsers)
		{
			selectedUsers.add(user.getUserId());
		}
		
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("userList", userService.listUsers());
		mav.addObject("selectedUserList", selectedUsers);
		mav.addObject("id", id.toString());
		mav.addObject("partial", "electionUsers");
		
		return mav;
	}

	@RequestMapping(value = "/electionUsers/postUsers/{id}", method = RequestMethod.POST)
	public ModelAndView postElectionUsers(final HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Integer id) {
		
		electionUserService.clearUsers(id);
		
		for (String value : request.getParameterValues("options"))
		{
			ElectionUser user = new ElectionUser();
			user.setUserId(Integer.parseInt(value));
			user.setElectionId(id);
			electionUserService.addElectionUser(user);
		}
		
		return new ModelAndView("redirect:/elections/");
	}
}
