package wauj.web.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import wauj.web.model.Roles;

/**
 * Helper class for security
 * 
 * @author Danijel
 *
 */
public class SecurityUtils {

	/**
	 * Check if user has role.
	 * 
	 * @param role
	 * @return
	 */
	public static boolean userHasRole(final Roles role) {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return false;
		}

		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return false;
		}

		for (GrantedAuthority auth : authentication.getAuthorities()) {
			if (role.toString().equals(auth.getAuthority())) {
				return true;
			}
		}

		return false;
	}
}
