<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="container">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="naslov">Prijava</div>
		<div class="panel panel-default">
			<div class="panel panel-body">
				<spring:url var="authUrl" value="/credentialsCheck" />
				<form method="post" action="<c:url value="${authUrl}"/>" name="formlogin" id="formlogin" data-validate="parsley" class="panel panel-body">
					<div class="form-group">
						<label class="control-label">E-mail:</label> <input type="text" name="j_username" id="login" placeholder="E-mail" data-required="true" class="form-control input-lg">
					</div>
					<div class="form-group">
						<label class="control-label">Lozinka:</label> <input type="password" name="j_password" id="password" placeholder="Lozinka" data-required="true" class="form-control input-lg">
					</div>
					<a href="forgot" class="pull-right m-t-xs"><small>Zaboravili ste lozinku?</small></a>
					<!-- Display security error if any -->
					<c:if test="${!empty SPRING_SECURITY_LAST_EXCEPTION}">
						<div id="alert" class="alert alert-danger">
							<i class="fa fa-ban-circle"></i>
							<c:out escapeXml="false" value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
						</div>
						<c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session" />
					</c:if>
					<button type="submit" class="btn btn-primary">Prijava</button>
				</form>
				<div class="register-block row panel-body wrapper-lg">
					<div class="col-sm-7">Za pristupanje glasanjima potrebno je imati valjani korisnički račun. Nakon registracije, Vaša lozinka će biti poslana na Vaš e-mail.</div>
					<div class="col-sm-5" style="text-align: center;">
						<a class="btn btn-default" href="register" role="button">Registriraj me</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>