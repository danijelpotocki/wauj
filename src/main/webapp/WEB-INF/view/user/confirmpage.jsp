<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<section class="col-sm-6 col-sm-offset-3 panel panel-default bg-white">
		<header class="row panel-heading panel-heading-big text-center">
			<strong>Potvrda računa</strong>
		</header>
		<spring:url var="confirmUrl" value="confirm" />
		<form method="post" action="<c:url value="${confirmUrl}"/>" name="formconfirm" id="formconfirm" class="panel-body wrapper-sm">
			<div class="form-group">
				<label class="control-label">Lozinka potvrde:</label>
				<input type="text" name="code" id="code" placeholder="Lozinka potvrde" data-required="true" class="form-control input-lg">
			</div>
			<button type="submit" class="btn btn-primary">Potvrdi</button>
		</form>
	</section>
</div>  