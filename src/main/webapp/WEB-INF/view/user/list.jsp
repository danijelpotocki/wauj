<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="container">
	<div class="naslov">Rezultati izbora</div>
	<div class="panel panel-default">
		<div class="panel panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Email</th>
						<th>Ime</th>
						<th>Prezime</th>
						<th>OIB</th>
						<th>Adresa</th>
						<th>Jedinstvena lozinka</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="user" items="${userList}" varStatus="status">
						<tr>
							<td>${user.userId}</td>
							<!-- ID -->
							<td>${user.email}</td>
							<!-- Email -->
							<td>${user.name}</td>
							<!-- Ime -->
							<td>${user.surname}</td>
							<!-- Prezime -->
							<td>${user.oib}</td>
							<!-- OIB -->
							<td>${user.address}</td>
							<!-- Adresa -->
							<td>${user.uniqueCode}</td>
							<!-- Jedinstvena lozinka -->
							<td><a class="btn btn-info btn-xs" href="/wauj/user/edit/${user.userId}" role="button">Uredi</a></td>
							<td><a class="btn btn-danger btn-xs" href="/wauj/user/remove/${user.userId}" role="button">Obriši</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>