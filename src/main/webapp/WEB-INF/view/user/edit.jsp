<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="container">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="naslov">Račun</div>
		<div class="panel panel-default">
			<div class="panel panel-body">

				<c:url var="registerUrl" value="http://localhost:8080/wauj/user/register" />

				<form:form method="POST" action="${registerUrl}" acceptCharset="UTF-8" name="formregister" id="formregister" data-validate="parsley" class="panel-body wrapper-lg" modelAttribute="userBean">

					<div class="row">
						<!-- left col -->
						<div class="col-sm-6">
							<c:choose>
							      <c:when test="${not empty userBean.userId}">
							      		<form:input path="userId" type="hidden" id="userId" data-required="true" class="form-control input-lg" />
							      </c:when>
							</c:choose>

							<div class="form-group">
								<label class="control-label">Ime:</label>
								<form:input path="name" type="text" id="name" placeholder="Ime" data-required="true" class="form-control input-md" />
							</div>
							<div class="form-group">
								<form:errors path="name">
									<div class="alert alert-danger">Ime se mora unijeti</div>
								</form:errors>
							</div>
							<div class="form-group">
								<label class="control-label">Prezime:</label>
								<form:input path="surname" type="text" id="surname" placeholder="Prezime" data-required="true" class="form-control input-md" />
							</div>
							<div class="form-group">
								<form:errors path="surname">
									<div class="alert alert-danger">Prezime se mora unijeti</div>
								</form:errors>
							</div>
							<div class="form-group">
								<label class="control-label">OIB:</label>
								<form:input path="oib" type="text" id="oib" placeholder="OIB" data-required="true" class="form-control input-md" />
							</div>
							<div class="form-group">
								<form:errors path="oib">
									<div class="alert alert-danger">OIB mora sadržavati točno 11 znakova</div>
								</form:errors>
							</div>
							<div class="form-group">
								<label class="control-label">Adresa:</label>
								<form:input path="address" type="text" id="address" placeholder="Adresa" data-required="true" class="form-control input-md" />
							</div>
						</div>

						<!-- right col -->
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">E-mail:</label>
								<form:input path="email" type="text" id="email" placeholder="E-mail" data-required="true" class="form-control input-md" />
							</div>
							<div class="form-group">
								<form:errors path="email">
									<div class="alert alert-danger">Email mora biti u ispravnom formatu</div>
								</form:errors>
							</div>
							<div class="form-group">
								<label class="control-label">Lozinka:</label> <input type="password" name="password" id="password" placeholder="Lozinka" data-required="true" class="form-control input-md">
								<form:errors path="password">
									<div class="alert alert-danger">Lozinka se mora unijeti</div>
								</form:errors>
							</div>
							<div class="form-group">
								<label class="control-label">Potvrdi lozinku:</label> <input type="password" name="password_confirm" id="password_confirm" placeholder="Potvrdi lozinku" data-required="true"
									class="form-control input-md"
								>
								<c:if test="${not empty passError}" >
								<div class="alert alert-danger">${passError}</div>
								</c:if>
							</div>
						</div>
					</div>

					<div class="row">
						<!-- Display security error if any -->
						<c:if test="${!empty SPRING_SECURITY_LAST_EXCEPTION}">
							<div id="alert" class="alert alert-danger">
								<i class="fa fa-ban-circle"></i>
								<c:out escapeXml="false" value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
							</div>
							<c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session" />
						</c:if>
					</div>
					<div class="row">
						<div class="col-xs-12">

							<c:choose>
								<c:when test="${not empty userBean.userId}">
									<c:url var="button" value="Spremi" />
								</c:when>
								<c:otherwise>
									<c:url var="button" value="Registriraj me" />
								</c:otherwise>
							</c:choose>

							<button type="submit" class="btn btn-primary pull-right">${button}</button>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>