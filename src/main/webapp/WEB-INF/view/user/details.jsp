<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="container">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="naslov">Moj račun</div>
		<div class="panel panel-default">
			<div class="panel panel-body">

				<table class="table table-hover ">
					<tbody>
						<tr>
							<th scope="row">Email</th>
							<td>${userBean.email}</td>
						</tr>
						<tr>
							<th scope="row">Ime</th>
							<td>${userBean.name}</td>
						</tr>
						<tr>
							<th scope="row">Prezime</th>
							<td>${userBean.surname}</td>
						</tr>
						<tr>
							<th scope="row">OIB</th>
							<td>${userBean.oib}</td>
						</tr>
						<tr>
							<th scope="row">Adresa</th>
							<td>${userBean.address}</td>
						</tr>
						<tr>
							<th scope="row">Jedinstveni ključ</th>
							<td>${userBean.uniqueCode}</td>
						</tr>
						<tr>
							<th scope="row"></th>
							<td><a class="btn btn-info pull-right" href="edit" role="button">Uredi</a></td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>