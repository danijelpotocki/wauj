<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
	<head>
		<meta charset="utf-8">
		<title>User data</title>
	</head> 
	<body>
		<h2>User data</h2>
		<div>Hello <security:authentication property="principal.username" />!</div>
		<div>
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<p>User id: ${userDataAttr.userId}</p>
			</security:authorize>

			<p>Email: ${userDataAttr.email}</p>
			<p>Name: ${userDataAttr.name}</p>
			<p>Surname: ${userDataAttr.surname}</p>
		</div>
	</body>
</html>