<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<section class="panel panel-default bg-white">
				<header class="panel-heading panel-heading-big text-center">
					<strong>Dodavanje izbora</strong>
				</header>
				<c:url var="submitUrl" value="postElection" />
				<form:form method="POST" action="${submitUrl}" name="formelection" id="formelection" data-validate="parsley" class="panel-body wrapper-md" modelAttribute="electionBean">
					<form:hidden path="electionId" />
					<div class="form-group">
						<label class="control-label">Naziv:</label>
						<form:input path="title" type="text" id="title" placeholder="Naziv" data-required="true" class="form-control input-md" />
					</div>
					<div class="form-group">
						<form:errors path="title"><div class="alert alert-danger">Naziv se mora unijeti</div></form:errors>
					</div>
					<div class="form-group">
						<label class="control-label">Opis:</label>
						<form:input path="description" type="text" id="description" placeholder="Opis" data-required="true" class="form-control input-md" />
					</div>
					<div class="form-group">
						<label class="control-label">Početak:</label>
						<div class='input-group date' id='datetimepicker1'>
							<form:input path="startDate" type="text" id="startDate" placeholder="Početak" data-required="true" class="form-control input-md" />
							<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<form:errors path="startDate"><div class="alert alert-danger">Početak mora biti validan datum i/ili vrijeme</div></form:errors>
					</div>
					<div class="form-group">
						<label class="control-label">Kraj:</label>
						<div class='input-group date' id='datetimepicker2'>
							<form:input path="endDate" type="text" id="endDate" placeholder="Kraj" data-required="true" class="form-control input-md" />
							<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group">
						<form:errors path="endDate"><div class="alert alert-danger">Kraj mora biti validan datum i/ili vrijeme</div></form:errors>
					</div>
					<button type="submit" class="btn btn-primary pull-right">Spremi / Dalje</button>
				</form:form>
			</section>
		</div>
	</div>

</div>

<script type="text/javascript">
	var startDate = $('#datetimepicker1 :input').val();
	var endDate = $('#datetimepicker2 :input').val();
	
	if(startDate != '' && startDate != null) startDate = moment(startDate, 'YYYY-MM-DD HH:mm:ss.S').format('DD/MM/YYYY');
	if(endDate != '' && endDate != null) endDate = moment(endDate, 'YYYY-MM-DD HH:mm:ss.S').format('DD/MM/YYYY');
	
	$(function() {
		$('#datetimepicker1').datetimepicker({
			viewMode : 'days',
			format : 'DD/MM/YYYY'
		});
	});
	
	$(function() {
		$('#datetimepicker2').datetimepicker({
			viewMode : 'days',
			format : 'DD/MM/YYYY'
		});
	});
	
	$('#datetimepicker1 :input').val(startDate);
	$('#datetimepicker2 :input').val(endDate);
</script>
