<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<section class="panel panel-default bg-white">
				<header class="panel-heading panel-heading-big text-center">
					<strong>Dodavanje opcija</strong>
					<div class="btn-back-wrapper">
						<a href="" id="backBtn"><span class="glyphicon glyphicon-menu-left btn-panel-back" aria-hidden="true"></span> Natrag</a>
					</div>
				</header>
				<div class="panel-body wrapper-md">
					<table id="electionOptions" class="table table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Naziv</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
						</tbody>
					</table>
					<div>
						<div class="input-group">
							<input type="text" id="optionTitle" class="form-control" placeholder="Upišite naziv..."> <span class="input-group-btn">
								<button class="btn btn-primary" type="button" id="addElectionOptionBtn" onclick="addElectionOption()">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Dodaj!
								</button>
							</span>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="register-block panel-body wrapper-lg">
					<a class="btn btn-primary pull-right" href="${pageContext.request.contextPath}/electionUsers/${id}">Spremi / Dalje</a>
				</div>
			</section>
		</div>
	</div>

</div>


<script>
	// Adding url to "natrag" button
	var baseUrl = '${pageContext.request.contextPath}';
	var href = window.location.href;
	var hrefID = href.substr(href.lastIndexOf('/') + 1);
	$('#backBtn').attr("href", baseUrl + '/elections/update/' + hrefID);

	// Add-enter script
	$("#optionTitle").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#addElectionOptionBtn").click();
			$("#optionTitle").focus();
		}
	});

	var updateTable = function(data) {
		// data array of arrays :D
		// 0 - electionOptionID
		// 2 - parent
		// 1 - Title
		// 3 - voteZ

		$('#electionOptions tbody').empty();

		data
				.forEach(function(item) {
					$('#electionOptions tbody')
							.append(
									'<tr><td>'
											+ item[0]
											+ '</td><td>'
											+ item[1]
											+ '</td><td><button class="btn btn-danger btn-xs pull-right" onclick="deleteElectionOption('
											+ item[0]
											+ ')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Obriši</button></td></tr>');
				});
	};

	var getElectionOptions = function() {
		$
				.ajax({
					url : '${pageContext.request.contextPath}/api/elections/options/getAll?electionId='
							+ window.location.href.split('/').pop(),
					method : 'POST',
					contentType : 'application/json;charset=utf-8',
					success : function(data) {
						console.log(data);
						updateTable(data);
					},
					error : function() {
						alert('Dogodila se greška prilikom dodavanja opcije.');
					}
				});
	}

	var addElectionOption = function() {
		var obj = {
			electionID : window.location.href.split('/').pop(),
			optionTitle : $('#optionTitle').val()
		}

		$('#optionTitle').val('');

		console.log("šaljem", obj);

		$
				.ajax({
					url : '${pageContext.request.contextPath}/api/elections/options/add?electionId='
							+ obj.electionID
							+ '&optionTitle='
							+ obj.optionTitle,
					method : 'POST',
					contentType : 'application/json;charset=utf-8',
					success : function(data) {
						updateTable(data);
					},
					error : function() {
						alert('Dogodila se greška prilikom dodavanja opcije.');
					}
				});
	}

	var deleteElectionOption = function(id) {
		$
				.ajax({
					url : '${pageContext.request.contextPath}/api/elections/options/deleteOption?electionOptionId='
							+ id,
					method : 'POST',
					contentType : 'application/json;charset=utf-8',
					success : function(data) {
						console.log(data);
						updateTable(data);
					},
					error : function() {
						alert('Dogodila se greška prilikom brisanja.');
					}
				});
	}

	getElectionOptions();
</script>
