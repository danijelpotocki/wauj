<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<section class="panel panel-default bg-white">
				<header class="panel-heading panel-heading-big text-center">
					<strong>Dodavanje glasača</strong>
					<div class="btn-back-wrapper">
						<a href="" id="backBtn"><span class="glyphicon glyphicon-menu-left btn-panel-back" aria-hidden="true"></span> Natrag</a>
					</div>
				</header>
				<c:url var="registerUrl" value="/electionUsers/postUsers/${id}" />
				<form:form method="POST" action="${registerUrl}" acceptCharset="UTF-8"
					name="formregister" id="formregister" data-validate="parsley">
				<div class="panel-body wrapper-lg">
					<select multiple name="options" class="multiple-select">
					   <c:forEach var="user" items="${userList}" varStatus="status">
					   		<c:set var="contains" value="false" />
							<c:forEach var="item" items="${selectedUserList}">
								<c:if test="${item eq user.userId}">
								  	<c:set var="contains" value="true" />
								</c:if>
							</c:forEach>
					    	<option value="${user.userId}" <c:if test="${contains}">selected="selected"</c:if>>${user.name} ${user.surname}</option>
					   </c:forEach>
					</select>
				</div>
				<div class="register-block panel-body wrapper-lg">
					<button type="submit" class="btn btn-primary pull-right">Spremi / Završi</button>
				</div>
				</form:form>
			</section>
		</div>
	</div>
</div>

<script>
	$('select').each(function() {
		$(this).attr('size', $(this).find('option').length)
	});
	
	// Adding url to "natrag" button
	var baseUrl = '${pageContext.request.contextPath}';
	var href = window.location.href;
	var hrefID = href.substr(href.lastIndexOf('/') + 1);
	$('#backBtn').attr("href", baseUrl + '/electionOptions/' + hrefID);

	// Add-enter script
	$("#optionTitle").keyup(function(event) {
		if (event.keyCode == 13) {
			$("#addElectionOptionBtn").click();
			$("#optionTitle").focus();
		}
	});
</script>
