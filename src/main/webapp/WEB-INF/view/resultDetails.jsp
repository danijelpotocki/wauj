<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="container">
	<div class="naslov">Rezultati izbora - detalji</div>
	<div class="row">
		<div class="col-md-7 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel panel-body">
					<table class="table table-hover ">
						<tbody>
							<tr>
								<th scope="row">Naziv</th>
								<td>${electionBean.title}</td>
							</tr>
							<tr>
								<th scope="row">Opis</th>
								<td>${electionBean.description}</td>
							</tr>
							<tr>
								<th scope="row">Početak</th>
								<td><fmt:formatDate value="${electionBean.endDate}" pattern="dd.MM.yyyy." /></td>
							</tr>
							<tr>
								<th scope="row">Kraj</th>
								<td><fmt:formatDate value="${electionBean.endDate}" pattern="dd.MM.yyyy." /></td>
							</tr>
							<tr>
								<th scope="row">Ukupan broj glasova</th>
								<td id="brojGlasova">-</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="panel panel-default" style="margin-top: 15px;">
				<div class="panel panel-body">
					<table id="electionOptions" class="table table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Naziv</th>
								<th>Broj glasova</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div id="chart"></div>
		</div>
	</div>
</div>



<script>
	var updateTable = function(data) {
		// data array of arrays :D
		// 0 - electionOptionID
		// 1 - parent
		// 2 - Title
		// 3 - voteZ

		var numberCount = 0;
		
		$('#electionOptions tbody').empty();

		data.forEach(function(item) {
			$('#electionOptions tbody').append('<tr><td>' + item[0]	+ '</td><td>' + item[1]
											+ '</td><th scope="row">' + item[2] + '</th></tr>');
			
			numberCount += item[2];
		});
		
		// number of votes
		$('#brojGlasova').html(numberCount);
	};

	var getElectionOptions = function() {
		$.ajax({
			url : '${pageContext.request.contextPath}/api/elections/options/getAll?electionId=' + window.location.href.split('/').pop(),
			method : 'POST',
			contentType : 'application/json',
			success : function(data) {
				console.log(data);
				updateTable(data);
			},
			error : function() {
				alert('Dogodila se greška.');
			}
		});
	}

	getElectionOptions();
	
	
	
	var pieData = [];
	var getChartData = function() {
	var chart;
		$.ajax({
			url : '${pageContext.request.contextPath}/api/elections/graph?electionId=' + window.location.href.split('/').pop(),
			method : 'POST',
			contentType : 'application/json',
			success : function(data) {
				data.forEach(function(item) {
					var helperArray = [];
					helperArray.push(item[1], item[2])
					pieData.push(helperArray);
				});
				console.log(pieData);
				chart = c3.generate({
				    data: {
				        // iris data from R
				        columns: pieData,
				        type : 'pie'
				    }
				});
			},
			error : function() {
				alert('Dogodila se greška.');
			}
		});
	}
	
	getChartData();
</script>

<script>



</script>