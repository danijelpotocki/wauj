<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css' />" type="text/css" />
<link rel="stylesheet" href="<c:url value='/resources/css/style.css' />" type="text/css" />
<script src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/d3.min.js" />"></script>

<title>Elections</title>
</head>
<body>
	<div class="container">
		<div class="naslov">
			Pregled izbora <a class="btn btn-primary pull-right" href="${pageContext.request.contextPath}/elections/add" style="margin-top:10px;">Dodaj izbor</a>
		</div>
		<p style="display:none;">
		    <jsp:useBean id="today" class="java.util.Date" />
		    <b><c:out value="${today}"/></b>
		</p>
		<div class="panel panel-default">
			<div class="panel panel-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Naziv</th>
							<th>Opis</th>
							<th>Početak</th>
							<th>Kraj</th>
							<th>Akcije</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="election" items="${electionList}" varStatus="status">
							<tr>
								<td>${election.electionId}</td>
								<!-- ID -->
								<td>${election.title}</td>
								<!-- Naziv -->
								<td>${election.description}</td>
								<!-- Opis -->
								<td><fmt:formatDate value="${election.startDate}" pattern="HH:mm dd-MM-yyyy" /></td>
								<!-- Početak -->
								<td><fmt:formatDate value="${election.endDate}" pattern="HH:mm dd-MM-yyyy" /></td>
								<!-- Kraj -->
								<td><c:if test="${election.startDate > today}"><a href="${pageContext.request.contextPath}/elections/update/${election.electionId}">Ažuriraj</a></c:if></td>
								<td><c:if test="${election.startDate > today}"><a href="${pageContext.request.contextPath}/elections/remove?id=${election.electionId}">Obriši</a></c:if></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>