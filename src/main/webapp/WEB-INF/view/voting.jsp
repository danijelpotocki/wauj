<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="container">
	<div class="naslov">Glasanje</div>
	<div class="panel panel-default">
		<div class="panel panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Naziv</th>
						<security:authorize access="hasRole('ROLE_ADMIN')">
							<th>Glasovi</th>
						</security:authorize>
						<security:authorize access="isAuthenticated()">
							<th class="table-actions">Akcije</th>
						</security:authorize>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="electionOption" items="${electionOptionsList}" varStatus="loop">
						<tr>
							<th scope="row">${loop.index}</th>
							<td>${electionOption.electionOptionId}</td>
							<td>${electionOption.title}</td>
							<security:authorize access="hasRole('ROLE_ADMIN')">
								<td>${electionOption.votes}</td>
							</security:authorize>
							<security:authorize access="isAuthenticated()">
								<td><a href="/wauj/elections/voting/addVote/${electionOption.electionOptionId}" class="btn btn-info btn-xs">Daj glas!</a></td>
							</security:authorize>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
	$('.btn-info').on('click', function(e) {
		if (confirm('Potvrdi.')) {
			if (confirm('Jeste li sigurni?')) {

			} else
				e.preventDefault();
		} else
			e.preventDefault();
	});

	var voteable = '${voteable}';
	console.log(voteable);
	if (voteable == 'false')
		$(".btn-info").attr("disabled", "disabled");
</script>