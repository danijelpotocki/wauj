<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css' />" type="text/css" />
<link rel="stylesheet" href="<c:url value='/resources/css/c3.css' />" type="text/css" />
<link rel="stylesheet" href="<c:url value='/resources/css/style.css' />" type="text/css" />
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-datepicker.min.css' />" type="text/css" />
<script src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"></script>
<script src="<c:url value="/resources/js/moment.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap-datetimepicker.js" />"></script>
<script src="<c:url value="/resources/js/d3.min.js" />"></script>
<script src="<c:url value="/resources/js/c3.min.js" />"></script>
<title>e-Glasovanje</title>
</head>
<body>
	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/index"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> e-Glasovanje</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="${pageContext.request.contextPath}/elections/active">Izbori u tijeku</a></li>
					<li><a href="${pageContext.request.contextPath}/results">Rezultati</a></li>
					<security:authorize access="hasRole('ROLE_ADMIN')">
						<li><a href="${pageContext.request.contextPath}/elections">Administriranje izbora</a></li>
					</security:authorize>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<security:authorize access="hasRole('ROLE_ADMIN')">
						<li><a href="${pageContext.request.contextPath}/user/list">Lista korisnika</a></li>
					</security:authorize>
					<security:authorize access="isAuthenticated()">
						<!-- Content for Authenticated users -->
						<li><a href="${pageContext.request.contextPath}/user/account">Moj račun</a></li>
						<li><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
					</security:authorize>
					<security:authorize access="!isAuthenticated()">
						<!-- Content for unAuthenticated users -->
						<li><a href="${pageContext.request.contextPath}/login">Prijava / Registracija</a></li>
					</security:authorize>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<section class="main">
		<!-- content goes here yo! -->
		<c:choose>
			<c:when test="${not empty partial}">
				<jsp:include page="${ partial }.jsp" />
			</c:when>
			<c:otherwise>
				<jsp:include page="info.jsp" />
			</c:otherwise>
		</c:choose>
	</section>
</body>
</html>