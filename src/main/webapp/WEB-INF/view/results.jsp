<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:formatDate value="${bean.date}" pattern="yyyy-MM-dd" />

<div class="container">
	<div class="naslov">Rezultati izbora</div>
	<div class="panel panel-default">
		<div class="panel panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Id</th>
						<th>Kraj</th>
						<th>Naziv</th>
						<th>Pobjednik</th>
						<th class="table-actions">Akcije</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="election" items="${electionsList}" varStatus="loop">
						<tr>
							<td scope="row">${loop.index}</td>
							<td>${election.electionId}</td>
							<td>${election.title}</td>
							<td>${election.description}</td>
							<td>${election.electionOptionTitle}</td>
							<td><a href="results/details/${election.electionId}">Detalji</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>