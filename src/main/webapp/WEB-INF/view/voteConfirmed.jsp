<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="naslov">Glas spremljen</div>
		<p>Hvala Vam na vašem glasu. Vaš glas je spremljen.</p>
		<p>
			Nakon završetka glasovanja, rezultati će biti objavljeni <a href="${pageContext.request.contextPath}/results">ovdje</a>.
		<p>
	</div>
</div>