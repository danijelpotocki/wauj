<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:formatDate value="${bean.date}" pattern="yyyy-MM-dd HH:mm:ss" />

<div class="container">
	<div class="naslov">Izbori u tijeku</div>
	<div class="panel panel-default">
		<div class="panel panel-body">
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Id</th>
				<th>Naziv</th>
				<th>Opis</th>
				<th>Početak</th>
				<th>Kraj</th>
				<th class="table-actions">Akcije</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="election" items="${activeElectionsList}" varStatus="status">
				<tr>
					<th scope="row">${election.electionId}</th>
					<!-- ID -->
					<td>${election.title}</td>
					<!-- Naziv -->
					<td>${election.description}</td>
					<!-- Opis -->
					<td><fmt:formatDate value="${election.startDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<!-- Početak -->
					<td><fmt:formatDate value="${election.endDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<!-- Kraj -->
					<td><a href="voting/${election.electionId}">Detalji</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
		</div>
	</div>
	
</div>